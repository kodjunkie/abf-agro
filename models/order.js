const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema(
	{
		name: {
			type: String,
			required: true
		},
		address: {
			type: String,
			required: true
		},
		quantity: Schema.Types.Number,
		user: {
			type: Schema.Types.ObjectId,
			required: true,
			ref: 'User'
		}
	},
	{ timestamps: true }
);

module.exports = mongoose.model('Order', orderSchema);
