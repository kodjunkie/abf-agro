# ABF Agro-Allied Enterprise

A simple poultry ordering system prototype application, using the Federal University of Technology, Minna as a case study.

## Installation

```bash
    git clone https://gitlab.com/Paplow/abf-agro.git
    cd abf-agro
    cp .env.example .env
    # Modify .env file accordingly
    npm install
    # Start your mongodb server
    npm start
```
