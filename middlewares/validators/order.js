const { body } = require('express-validator/check');

exports.createOrder = [
	body('item', 'Item is not selected.').trim(),
	body('qty').isInt(),
	body('address', 'Address must not be empty.').trim()
];
