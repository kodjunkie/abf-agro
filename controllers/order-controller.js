const { validationResult } = require('express-validator/check');

const render = require('../util/render');
const Order = require('../models/order');
const { title } = require('../util/page');

/**
 * POST /orders
 * Order
 * @param  {} req
 * @param  {} res
 * @param  {} next
 */
exports.postOrder = async (req, res, next) => {
	const inputs = req.body;
	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		return render('user/dashboard', req, res, {
			pageTitle: title('Dashboard'),
			validatorErrors: errors.array()
		});
	}

	try {
		const order = new Order({
			name: inputs.item,
			address: inputs.address,
			user: req.user._id,
			quantity: inputs.qty
		});

		await order.save();

		req.flash('success', 'Order placed successfully.');
		return res.redirect('/');
	} catch (err) {
		next(err);
	}
};
