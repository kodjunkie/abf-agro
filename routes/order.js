const express = require('express');

const orderController = require('../controllers/order-controller');
const validator = require('../middlewares/validators/order');
const { isAuth } = require('../middlewares/user');

const router = express.Router();

router.post('/', isAuth, validator.createOrder, orderController.postOrder);

module.exports = router;
